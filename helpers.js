const colorRandomizer = () => {
	return Math.floor(Math.random()*16777215).toString(16)
}

// if you have several functions to export, you can use the export object

export { colorRandomizer };