import { useState, useEffect } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';
import View from '../../components/View';
import AppHelper from '../../app-helper';


export default function AddCategory() {

    return ( 
        <View title={ 'Add New Category' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>Category Information</h3>
                    <AddForm />
                </Col>
            </Row>
        </View>
    )

}


const AddForm = () => {
	const [name, setName] = useState('');
	const [type, setType] = useState('');

	function addCategory(e) {
		e.preventDefault();

		fetch('http://localhost:4000/api/users/add-category', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ AppHelper.getAccessToken() }`
			},
			body: JSON.stringify({
				name: name,
				type: type
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				setName('');
				setType('');

				Swal.fire({
					  icon: 'success',
					  title: 'Category successfully added',
					  showConfirmButton: false,
					  timer: 1500
					})

				Router.push('/categories')
			}

		})
	}

	return(

		<Card>
            <Card.Header>Add Category</Card.Header>
            <Card.Body>
                <Form onSubmit={e => addCategory(e)}>
                    <Form.Group controlId="name">
                        <Form.Label>Category Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            value={ name } 
                            onChange={(e) => setName(e.target.value)} 
                            autoComplete="off" 
                            required
                        />
                    </Form.Group>
                    <Form.Group controlId="type">
                        <Form.Label>Category Type</Form.Label>
                        <Form.Control 
                        	as="select" 
                        	value={ type } 
                            onChange={ (e) => setType(e.target.value) }  
                            required>
                                <option value="" disabled selected>Select Category</option>
						      	<option>Income</option>
						      	<option>Expense</option>
					    </Form.Control>
                    </Form.Group>
                    
                    <Button 
                        className="mb-2"
                        variant="info"
                        type="submit" 
                    >
                        Submit
                    </Button>
                    
                </Form>
            </Card.Body>
        </Card>

	)
}
